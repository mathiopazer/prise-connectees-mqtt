#!/usr/bin/python3
# coding: utf8
import sqliteConnector
import json
import pub
import datetime
global plages





def update():
	plages = json.loads(sqliteConnector.getPlagesHoraires())
	date = datetime.datetime.now()
	for x in plages:
		debut = x["debut"]
		fin = x["fin"]
		adresseMAC = x["MAC"]
		print(adresseMAC, debut, fin)
		heure = debut.split(':')[0]
		minute = debut.split(':')[1]
		if (heure == str(date.hour)):
			if (minute == str(date.minute)):
				topic = "control/" + adresseMAC
				payload = '{"type":"order","state":1}'
				pub.pubMQTT(topic, payload)
				print("plage enable : " + heure + ":" + minute + " sur", topic)
		heure = fin.split(':')[0]
		minute = fin.split(':')[1]
		if (heure == str(date.hour)):
			if (minute == str(date.minute)):
				topic = "control/" + adresseMAC
				payload = '{"type":"order","state":0}'
				pub.pubMQTT(topic, payload)
				print("plage disable : " + heure + ":" + minute + " sur", topic)
	print('\n')
				
update()