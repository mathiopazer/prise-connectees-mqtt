import paho.mqtt.publish as publish
import configMQTT
#import argparse
import base64
#parser = argparse.ArgumentParser()

#parser.add_argument('--topic', action='store', dest='topic', help='topic MQTT')
#parser.add_argument('--payloadB64', action='store', dest='payloadB64', help='payload MQTT en B64')
#args = parser.parse_args()

authDict={'username': configMQTT.username, 'password': configMQTT.password}

def pubMQTT(topic, payload):
	publish.single(topic, payload, hostname=configMQTT.hostname, port=configMQTT.port, auth=authDict)
	
	

#if (args.topic) and (args.payloadB64):
#	pubMQTT(args.topic, args.payloadB64.b64decode())
#else:
#	print("Argument manquant")