# Place your local configuration in /etc/mosquitto/conf.d/
#
# A full description of the configuration file is at
# /usr/share/doc/mosquitto/examples/mosquitto.conf.example

pid_file /var/run/mosquitto.pid

persistence true
persistence_location /var/lib/mosquitto/

log_dest file /var/log/mosquitto/mosquitto.log

include_dir /etc/mosquitto/conf.d

#log_type debug
allow_anonymous false

password_file /etc/mosquitto/acl.txt

#log_type all

listener 9004


websockets_log_level 255
listener 9001
protocol websockets
cafile /home/pi/ssl/chain.pem
keyfile /home/pi/ssl/privkey.pem
certfile /home/pi/ssl/cert.pem

